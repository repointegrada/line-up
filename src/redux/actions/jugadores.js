import * as types from '../types/actionsJugador'

const setSuplente = (jugador) => (dispatch) => {
    dispatch({
        type: types.AGREGAR_SUPLENTE,
        payload: jugador
    })
}

const quitarSuplente = (jugador) => (dispatch) => {
    dispatch({
        type: types.QUITAR_SUPLENTE,
        payload: jugador
    })
}


const setPlayerWithDispatch = (jugador) => (dispatch) => {
    dispatch({
        type: types.AGREGAR_TITULAR,
        payload: jugador
    })
}

const quitarTitular = (jugador) => (dispatch) => {
    dispatch({
        type: types.QUITAR_TITULAR,
        payload: jugador
    })
}

const PlayerActions = {
    quitarSuplente,
    setPlayerWithDispatch,
    setSuplente,
    quitarTitular
}

export default PlayerActions