import jugadores from '../../assets/mockups/jugadores'
import { AGREGAR_TITULAR, AGREGAR_SUPLENTE, QUITAR_TITULAR, QUITAR_SUPLENTE } from '../types/actionsJugador'

const initialState = jugadores

// Pueden haber varias funciones reductoras se puede combinar. Usar combineReducer cuando hay más de 1 función en redux
// state: Estado actual de la app. Cuando se carga por primera vez no puede ser undefined y no se puede retornar
// action: Indica que cambio se debe hacer en el estado. Se lee la acción para ejecutar un cambio en el estado
const reducerEntrenador = (state = initialState, action) => {
    switch (action.type) {
        case AGREGAR_TITULAR:
            return {
                ...state,
                titulares: state.titulares.concat(action.payload),
                jugadores: state.jugadores.filter(jugador => jugador.id !== action.payload.id)
            }
        case AGREGAR_SUPLENTE:
            return {
                ...state,
                suplentes: state.suplentes.concat(action.payload),
                jugadores: state.jugadores.filter(jugador => jugador.id !== action.payload.id)
            }
        case QUITAR_TITULAR:
            return {
                ...state,
                titulares: state.titulares.filter(jugador => jugador.id !== action.payload.id),
                jugadores: state.jugadores.concat(action.payload)
            }
        case QUITAR_SUPLENTE:
            return {
                ...state,
                suplentes: state.suplentes.filter(jugador => jugador.id !== action.payload.id),
                jugadores: state.jugadores.concat(action.payload)
            }
        default:
            return state
    }
}

export default reducerEntrenador