// Redux
import { createStore, applyMiddleware, compose } from "redux";
import reducerEntrenador from './reducers/reducerEntrenador'
import thunk from "redux-thunk";

const composeEnchancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    reducerEntrenador,
    composeEnchancers(applyMiddleware(thunk))
);

export default store
