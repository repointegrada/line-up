const jugadores = {
    jugadores: [
        {
            id: 1,
            nombre: "Wuilker Fariñez",
            foto: "https://files.lafm.com.co/assets/public/styles/image_631x369/public/2019-06/farinez_wuilker_3_0.jpg?itok=zWJbq8Oc"
        },
        {
            id: 2,
            nombre: "Elvis Perlaza",
            foto: "https://primertiempo.co/wp-content/uploads/2020/02/EQI5oAYW4AA5sa5.jpg"
        },
        {
            id: 3,
            nombre: "Breiner Paz",
            foto: "https://pbs.twimg.com/media/DXAtJ_eX4AEy6yz.jpg"
        }, {
            id: 4,
            nombre: "Juan Pablo Vargas",
            foto: "https://as02.epimg.net/colombia/imagenes/2020/01/20/futbol/1579521623_707138_1579521974_noticia_normal_recorte1.jpg"
        },
        {
            id: 5,
            nombre: "Omar Bertel",
            foto: "https://as00.epimg.net/img/comunes/fotos/fichas/deportistas/o/oma/large/40634.png"
        },
        {
            id: 6,
            nombre: "Jhon Duque",
            foto: "https://as01.epimg.net/colombia/imagenes/2019/02/20/futbol/1550698886_492318_1550700610_noticia_normal.jpg"
        }, {
            id: 7,
            nombre: "Juan Pereira",
            foto: "https://gol.caracoltv.com/sites/default/files/juan_carlos_pereira_030120_tw_millosfcoficial_e_0.jpg"
        },
        {
            id: 8,
            nombre: "David Silva",
            foto: "https://postobon.s3.amazonaws.com/2019/03/David-Macalister-Millonarios.jpg"
        },
        {
            id: 9,
            nombre: "Ayron Del Valle",
            foto: "https://deportesimagenes.canalrcn.com/ImgDeportesNew/ayron_gol.jpg"
        }, {
            id: 10,
            nombre: "Hansel Zapata",
            foto: "https://www.futbolred.com/files/article_main/files/crop/uploads/2020/02/06/5e3cccef392e2.r_1581058383037.0-51-3000-1551.jpeg"
        }, {
            id: 11,
            nombre: "Jose Ortíz",
            foto: "https://www.elespectador.com/sites/default/files/ortiz_1.jpg"
        }, {
            id: 12,
            nombre: "Diego Godoy",
            foto: "https://gol.caracoltv.com/sites/default/files/diegogodoy_160120twmillosfcoficiale.png"
        },
        {
            id: 13,
            nombre: "César Carrillo",
            foto: "https://as00.epimg.net/img/comunes/fotos/fichas/deportistas/c/ces/large/29177.png"
        },
        {
            id: 14,
            nombre: "Cristian Arango",
            foto: "https://upload.wikimedia.org/wikipedia/commons/4/48/Chicho_arango_millos.png"
        },
        {
            id: 15,
            nombre: "Jefferson Martínez",
            foto: "https://cr00.epimg.net/radio/imagenes/2019/08/01/el_vbar/1564695463_082839_1564696320_noticia_normal_recorte1.jpg"
        },
        {
            id: 16,
            nombre: "Felipe Banguero",
            foto: "https://as00.epimg.net/img/comunes/fotos/fichas/deportistas/f/fel/large/29124.png"
        },
        {
            id: 17,
            nombre: "Felipe Román",
            foto: "https://ssl.c.photoshelter.com/img-get2/I000089QULVyrqdI/fit=1000x750/g=G0000oCf0HVXSULQ/Andres-Felipe-Roman-01.jpg"
        },
        {
            id: 18,
            nombre: "Óscar Barreto",
            foto: "https://as00.epimg.net/img/comunes/fotos/fichas/deportistas/o/osc/large/28646.png"
        },
        {
            id: 19,
            nombre: "Eliser Quiñones",
            foto: "https://www.futbolred.com/files/article_main/files/crop/uploads/2019/02/14/5c65bcadf21cb.r_1550241928864.0-0-948-475.jpeg"
        },
        {
            id: 20,
            nombre: "Stiven Vega",
            foto: "https://www.elespectador.com/sites/default/files/2fa606a1caa2ca0c11912f0a667c0758_2.jpg"
        },
        {
            id: 21,
            nombre: "Juan Salazar",
            foto: "https://upload.wikimedia.org/wikipedia/commons/8/8b/Juan-camilo-salazar-800x450.jpg"
        },
        {
            id: 22,
            nombre: "Santiago Montoya",
            foto: "https://futbolete.com/wp-content/uploads/2018/02/santiago-montoya-munoz-millonarios-superliga-1200x900.jpg"
        },
        {
            id: 23,
            nombre: "Deivy Balanta",
            foto: "https://admin.winsports.co/sites/default/files/videos/estandar/deivy_s.jpg"
        },

    ],
    titulares: [],
    suplentes: [],
    equipo: 'Millonarios FC',
    escudo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Escudo_de_Millonarios_temporada_2017.png/1200px-Escudo_de_Millonarios_temporada_2017.png'
}

export default jugadores