import React from 'react';
import { Provider } from 'react-redux'
import Jugadores from './components/Jugadores'
import EquipoSeleccionado from './components/EquipoSeleccionado'
import './styles/styles.scss'
import store from './redux/store';
import jugadores from './assets/mockups/jugadores'

// Provider debe envolver la app y proveer el store de la app
const App = () => {
  return (
    <Provider store={store}>
      <main>
        <div className="info-equipo">
          <img src={jugadores.escudo} alt="escudo" />
          <h1>{jugadores.equipo}</h1>
        </div>
        <Jugadores />
        <EquipoSeleccionado />
      </main>
    </Provider>
  )
}

export default App;
