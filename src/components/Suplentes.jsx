import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import PlayerActions from '../redux/actions/jugadores'

// import { connect } from 'react-redux'

const Suplentes = () => {
    // const Suplentes = ({ suplentes, quitarSuplente }) => {

    const dispatch = useDispatch()
    const suplentes = useSelector(state => state.suplentes)


    return (
        <section>
            <h2>Suplentes</h2>
            <div className="suplentes">
                {suplentes.map(suplente => (
                    <article className="suplente" key={suplente.id}>
                        <div>
                            <img src={suplente.foto} alt="suplente" />
                            {/* <button onClick={() => quitarSuplente(suplente)}>X</button> */}
                            <button onClick={() => dispatch(PlayerActions.quitarSuplente(suplente))}>X</button>
                        </div>
                        <p>{suplente.nombre}</p>
                    </article>
                ))}
            </div>
        </section>
    )
}

// const mapStateToProps = state => ({
//     // Del estado que recibe obtenga la prop
//     suplentes: state.suplentes
// })

// //Se encarga de llevar las acciones que van a ser leidas por el reducer
// const mapDispatchToProps = dispatch => {
//     return {
//         quitarSuplente: jugador =>
//             dispatch({
//                 type: "QUITAR_SUPLENTE",
//                 payload: jugador
//             })

//     }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(Suplentes)

export default Suplentes
