import React, { createRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import PlayerActions from '../redux/actions/jugadores'

const Jugadores = () => {

    const dispatch = useDispatch()
    const jugadores = useSelector(state => state.jugadores)

    const gridJugadores = createRef()

    useEffect(() => {
        setScrollContainer()
        document.addEventListener('click', setScrollContainer)
    })

    // Función que fija el tamaño del grid de los jugadores
    const setScrollContainer = (desktop = true) => {
        let container = gridJugadores.current
        if (container) {
            const generatedGrid = () => {
                let items = container.getElementsByClassName('jugador')
                let itemsLength = items.length
                let bp = window.matchMedia("(min-width: 640px)").matches ? window.matchMedia("(min-width: 1024px)").matches ? 4 : 2 : 1

                const getWidth = () => {
                    let n = bp + (itemsLength > bp ? 0.3 : 0)
                    return (itemsLength / n) * 100
                }
                return `
                  display: grid;
                  grid-template-columns: repeat(${itemsLength}, 225px);
                  grid-gap: 1rem;
                  width : ${getWidth()}%;
                `
            }
            let styles = !desktop && window.matchMedia("(min-width: 1024px)").matches ? `display: grid; grid-row-gap: 1rem;` : generatedGrid()
            container.setAttribute('style', styles)
        }
    }

    return (
        <section>
            <h1>{jugadores.length} Jugadores</h1>
            <div className="contenedor-jugadores">
                {jugadores.map(jugador => (
                    <article className="jugador" key={jugador.id}>
                        <img src={jugador.foto} alt="Foto de jugador" />
                        <h3>{jugador.nombre}</h3>
                        <div>
                            <button onClick={() => dispatch(PlayerActions.setPlayerWithDispatch(jugador))}>Titular</button>
                            <button onClick={() => dispatch(PlayerActions.setSuplente(jugador))}>Suplente</button>
                        </div>
                    </article>
                ))}
            </div>
        </section>
    )
}

// Recibe el estado para retornar un objeto
// const mapStateToProps = state => ({
//     // Del estado que recibe obtenga la prop
//     jugadores: state.jugadores
// })

// //Se encarga de llevar las acciones que van a ser leidas por el reducer
// const mapDispatchToProps = dispatch => {
//     return {
//         agregarTitular: jugador =>
//             // El siguiente objeto es un action
//             dispatch(PlayerActions.setPlayer(jugador)),
//         agregarSuplente: jugador =>
//             // El siguiente objeto es un action
//             dispatch({
//                 type: "AGREGAR_SUPLENTE",
//                 payload: jugador
//             })
//     }
// }

// connect recibe 2 funciones
// 1 la primera mapea lo que tiene en el estado y lo convierte en props
// 2 mapea las funciones (dispatch) y convierte en props
// Si no se usa alguna de las funciones se pasa un objeto vacío {}
// export default connect(mapStateToProps, mapDispatchToProps)(Jugadores)
export default Jugadores
