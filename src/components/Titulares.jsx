import React from 'react'
import cancha from '../assets/images/cancha.svg'
import { useDispatch, useSelector } from 'react-redux'
import PlayerActions from '../redux/actions/jugadores'

const Titulares = () => {
    
    // Dispara acciones
    const dispatch = useDispatch()
    // Extrae info del store de redux. Sustituye al mapStateToProps
    const titulares = useSelector(state => state.titulares)

    return (
        <section>
            <h2>Titulares</h2>
            <div className="cancha">
                {titulares.map(titular => (
                    <article className="titular" key={titular.id}>
                        <div>
                            <img src={titular.foto} alt="Titular" />
                            <button onClick={() => dispatch(PlayerActions.quitarTitular(titular))}>X</button>
                        </div>
                        <p>{titular.nombre}</p>
                    </article>
                ))}
                <img src={cancha} alt="cancha" />
            </div>
        </section>
    )
}

export default Titulares
